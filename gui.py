from queue import Queue
from tkinter import *


class Gui:
    window = None
    scrollbar = None
    textfield = None

    def __init__(self, title):
        self.window = Tk()
        self.window.title(title)
        self.window.iconphoto(False, PhotoImage(file="res/window_icon.png"))
        self.scrollbar = Scrollbar(self.window)
        self.textfield = Text(self.window, height=5, width=80)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        self.textfield.pack(side=LEFT, fill=Y)
        self.scrollbar.config(command=self.textfield.yview)
        self.textfield.config(yscrollcommand=self.scrollbar.set)
        # self.output_mode = self.OutputMode.GUI
        # self.cus_print(title + " started.")

    def start(self):
        self.window.mainloop()
