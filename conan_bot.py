
# conan_bot.py
import os
from client import ConanClient
from dotenv import load_dotenv
from threading import Thread
from gui import Gui

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')
STEAMAPPS_FOLDER = os.getenv('STEAMAPPS_FOLDER')


# init gui
gui = Gui("Conan Exiles Server Discord Bot")
# init client
client = ConanClient()
client.token = TOKEN
client.gui = gui
# start client in thread
client_thread = Thread(target=client.start_client)
client_thread.start()
# run gui loop
gui.start()
