
# client.py
import discord
import subprocess
import asyncio
import os
import urllib.request
from enum import Enum
from datetime import datetime
from tkinter import *


class ConanClient(discord.Client):
    # CONFIG
    # -- conan server --
    CONAN_SERVER_PATH = "C:/steamcmd/steamapps/common/Conan Exiles Dedicated Server/"
    SERVER_START_CMD = 'Conan-Server-Start.bat'
    CONAN_SS_EXE = "ConanSandboxServer.exe"
    CONAN_SS_WIN64 = "ConanSandboxServer-Win64-"
    # -- bot settings --
    STATUS_UPDATE_PERIOD = 60
    # -- discord setting --
    token = None
    # set these to the channel ids of your server
    # main channel gets input and responds to commands
    MAIN_CHANNEL_ID = 719121294200930444
    # dev channel gets input and responds with extended debugging notifications
    DEV_CHANNEL_ID = 771693292513394699
    # -- authorization --
    # set this to True to restrict certain commands to only be available to the specified authorized users
    AUTHORIZATION_REQUIRED = False
    AUTHORIZED_USERS = ["Josch5", "Chaz"]
    # -- messaging --
    # if this is set to True the bot sends a message when the conan server changes status
    MESSAGE_ON_STATUS_UPDATE = False

    # DEV CONFIG
    # -- debug mode --
    # if enabled send output to dev channel
    debug = False

    # CLASS VARIABLES
    # -- server status enumeration type --
    class Status(Enum):
        UNKNOWN = -1
        OFFLINE = 0
        ONLINE = 1

    # -- output mode enumeration type --
    class OutputMode(Enum):
        UNDEFINED = -1
        STANDARD = 0
        GUI = 1
    output_mode = OutputMode.STANDARD
    # -- server status vars --
    server_status = Status.UNKNOWN
    server_status_string = ""
    server_ip = ""
    # -- server channel vars --
    channel = None
    dev_channel = None
    # -- GUI vars --
    gui = None

    # post connected function
    async def on_ready(self):
        # init GUI print
        if self.gui is not None:
            self.output_mode = self.OutputMode.GUI
        self.cus_print(f'{self.user} has connected to Discord!')
        await self.init_channels()
        # initial server status update
        await self.update_server_status()
        await self.update_server_ip()
        # await self.message_server_status()

    async def init_channels(self):
        self.dev_channel = self.get_channel(self.DEV_CHANNEL_ID)
        if self.debug:
            self.channel = self.get_channel(self.DEV_CHANNEL_ID)
        else:
            self.channel = self.get_channel(self.MAIN_CHANNEL_ID)

    def start_client(self):
        if self.token is None:
            print("Token not set.")
            exit(-1)
        self.run(self.token)

    def gui_print(self, text):
        now = datetime.now()
        timestamp = now.strftime("%H:%M:%S")
        text = "(" + timestamp + "): " + text + "\n"
        self.gui.textfield.insert(END, text)
        self.gui.textfield.pack()

    def cus_print(self, text):
        if self.output_mode == self.OutputMode.STANDARD:
            print(text)
        elif self.output_mode == self.OutputMode.GUI:
            self.gui_print(text)
        else:
            if self.output_mode == self.OutputMode.UNDEFINED:
                print("Error: OutputMode set to UNDEFINED.")
            else:
                print("Error: Unknown OutputMode.")

    async def on_message(self, message):
        if message.author == self.user:
            return
        mes = message.content.lower()
        if mes == "!status":
            self.cus_print("Server status request (" + message.author.name + ").")
            await self.update_server_status()
            await self.message_server_status()
        if mes == "!start":
            self.cus_print("Server start request (" + message.author.name + ").")
            await self.update_server_status()
            if self.server_status == self.Status.ONLINE:
                await self.channel.send(f'Der Conan Exiles Server ist bereits {self.server_status_string}.')
            else:
                await self.start_server(message.author)
        if mes == "!stop":
            self.cus_print("Stop server request (" + message.author.name + ").")
            await self.stop_server(message.author)
        if mes == "!ip":
            self.cus_print("Server IP request (" + message.author.name + ").")
            await self.update_server_ip()
            await self.channel.send(f'Die IPv4 des Servers ist {self.server_ip} (Port: 7777).')
        if mes == "!update":
            self.cus_print("Server Update request..")
            if self.server_status == self.Status.ONLINE:
                await self.channel.send(f'Der Conan Exiles Server ist {self.server_status_string}. Um den Server zu '
                                        f'aktualisieren, beende erst den Server und führe, dann das Update durch.')
            else:
                await self.update_server()
        if mes == "!help":
            self.cus_print("Help request (" + message.author.name + ").")
            await self.channel.send(f'!status - Server Status abfragen (automatisch 1/min)\n!start - Server starten\n'
                                    f'!stop - Server schließen\n!ip - Die IPv4-Adresse des Servers abrufen')

    async def start_server(self, author):
        self.cus_print("Starting server ...")
        # os.chdir(self.CONAN_SERVER_PATH)
        subprocess.call([r'Conan-Server-Start.bat'])
        await self.channel.send(f'Der Conan Exiles Server wird für dich gestartet {author.name}.')

    async def stop_server(self, author):
        if self.AUTHORIZATION_REQUIRED:
            self.cus_print("Check author rights..")
            if author.name not in self.AUTHORIZED_USERS:
                await self.channel.send(f'Du bist leider nicht autorisiert den Server zu schließen.')
                return
        out = subprocess.Popen(['tasklist'],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
        stdout, stderr = out.communicate()
        lines = stdout
        lines = lines.splitlines()
        # self.cus_print(lines)
        css_exe_pid = ""
        css_win64_pid = ""
        for line in lines:
            line = str(line)
            if self.CONAN_SS_EXE in line:
                tokens = line.split()
                # for t in tokens:
                #     self.cus_print(t)
                css_exe_pid = tokens[1]
                self.cus_print("Server process: " + self.CONAN_SS_EXE + ", ID: " + css_exe_pid + ".")
            if self.CONAN_SS_WIN64 in line:
                tokens = line.split()
                # for t in tokens:
                #    self.cus_print(t)
                css_win64_pid = tokens[1]
                self.cus_print("Server process: " + self.CONAN_SS_WIN64 + ", ID: " + css_win64_pid + ".")
        if css_exe_pid != "" or css_win64_pid != "":
            # kill_exe = 'taskkill /T /PID ' + str(css_exe_pid)
            kill_win64 = 'taskkill /T /PID ' + str(css_win64_pid)
            self.cus_print("Kill Win64 call: " + kill_win64)
            os.system(kill_win64)
            # os.popen(kill_win64).readlines()
            # self.cus_print(str(stdout))
            self.cus_print("Server was shut down.")

    async def update_server(self):
        self.cus_print("Updating server..")
        subprocess.call([r'Conan-Server-Update.bat'])
        await self.channel.send(f'Gegebenenfalls werden verfügbare Updates installiert (Server muss für jeden Patch von CE aktualisiert werden).')

    async def server_status_task(self):
        while True:
            status = self.server_status
            await self.update_server_status()
            if self.MESSAGE_ON_STATUS_UPDATE:
                if self.server_status != status:
                    await self.message_server_status()
            await asyncio.sleep(self.STATUS_UPDATE_PERIOD)
            # self.cus_print("Loop")

    async def update_server_status(self):
        out = subprocess.Popen(['tasklist'],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
        stdout, stderr = out.communicate()
        # self.cus_print(stdout)
        tasklist = str(stdout)

        if self.CONAN_SS_EXE not in tasklist or self.CONAN_SS_WIN64 not in tasklist:
            self.server_status = self.Status.OFFLINE
        else:
            self.server_status = self.Status.ONLINE

        await self.update_server_status_string()
        await self.update_presence()

    async def update_server_status_string(self):
        if self.server_status == self.Status.UNKNOWN:
            self.server_status_string = "unknown"
        elif self.server_status == self.Status.OFFLINE:
            self.server_status_string = "offline"
        elif self.server_status == self.Status.ONLINE:
            self.server_status_string = "online"

    async def update_server_ip(self):
        external_ip = urllib.request.urlopen('https://v4.ident.me/').read().decode('utf8')
        self.cus_print('Updated Server IPv4: ' + external_ip)
        self.server_ip = external_ip

    async def message_server_status(self):
        await self.channel.send(f'Der Conan Exiles Server ist {self.server_status_string}.')

    async def update_presence(self):
        if self.server_status == self.server_status.ONLINE:
            await self.change_presence(activity=discord.Game('Server online'), status="online")
        else:
            await self.change_presence(activity=discord.Game('Server offline'), status="idle")

    # async def on_error(self, event_method, *args, **kwargs):
    #     await self.dev_channel.send(f'{self.user} has encountered an Error.\n Error log: {event_method}, {args}, {kwargs}')

