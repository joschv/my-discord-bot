# Conan Exiles Server Discord Bot

A discord bot that shows the status (Online/Offline) of a local (running on the same system) dedicated Conan Exiles Server.
It provides additional commands that let users:

 1. start the server remotely,
 2. shut it down remotely,
 3. update it remotely,
 4. get the IPv4 of the game server.
 
 Now runs with a small GUI for server logs.

 To set up your bot:

 0. Install Steamcmd and Conan Exiles Dedicated Server tool
 1. Create a .env file (in the project folder) with your DISCORD_TOKEN = xxx and the DISCORD_GUILD = xxx
 2. Ensure that the CONAN_SERVER_PATH variable matches your location of Steamcmd + Conan Server Installation
 